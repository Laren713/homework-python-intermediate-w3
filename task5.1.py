user_new_file_name = input('Enter the name of new file: ')
user_original_file_name = input('Enter the name of original file: ')

end_line = len(user_original_file_name)
if user_original_file_name[end_line-4:end_line] != '.txt':
    user_original_file_name = user_original_file_name + '.txt'

count = 0

with open(user_original_file_name, 'r') as origin_file:
    with open(user_new_file_name, 'a') as new_file:
        while True:
            count += 1
            line = origin_file.readline()

            if not line:
                break
            new_file.write(f"Line{count}: {line.strip()}\n")

with open(user_new_file_name, 'r') as f:
    print(f.read())
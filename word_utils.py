def remove_punctuation(some_string):
    return some_string.strip('?!#@$%^&*():;/-.,\><_+ ')


def words_from_string(some_string):
    return some_string.split()


def max_length_word(some_string):
    lst = some_string.split()
    for i in lst:
        if len(lst[0]) > len(i):
            continue
        else:
            lst[0] = i
    return lst[0]


print(remove_punctuation(' !!!^&%^)*Remove punctuation!?.,/:;! '))
print(words_from_string('Get words from string'))
print(max_length_word('Get max length word'))